/*
===============================================================================
 Name        : tp-tds-zolezzi.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "chip.h"
#include "timer_17xx_40xx.h"

typedef unsigned short uint16_t;
typedef unsigned char uint8_t;

#define TIC_1MS 			(1000)	// Tick del sistema - 1ms
#define TIC_LED_DEFAULT		(512)
#define LED_ROJO_PORT		(0)
#define LED_ROJO_PIN 		(22)
#define LED_PORT			(1)
#define LED_PIN 			(18)
#define BTN_EEPROM_PORT		(1)
#define BTN_EEPROM_PIN 		(19)
#define BTN_LED_PORT		(1)
#define BTN_LED_PIN 		(20)
#define TICK_BASE_LED		(512)
#define I2C_DEVICE			(I2C1)
#define I2C_DEVICE_NUM		(1)
#define I2C_DEVICE_PORT		(0)
#define I2C_SDA_PIN			(19)
#define I2C_SCL_PIN			(20)
#define I2C_SPEED			(100000)
#define EEPROM_ADDRESS		(0x20)
#define DEBOUNCE			6
#define dwt_init() 			{DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk; DWT->CYCCNT=0;}
#define dwt_reset() 		{DWT->CYCCNT=0;}
#define dwt_read() 			(DWT->CYCCNT)

void InitHardware(void);
void SetPinMode ( uint8_t puerto , uint8_t bit , uint8_t modo );
void TareaWDT(void);
void TareaTickLed(void);
void TareaBtnLed(void);
void TareaBtnEeprom(void);
uint32_t leer_24lc64(uint16_t address, int len, uint8_t *buf);
uint32_t escribir_24lc64(uint16_t address, uint32_t len, uint8_t *buf);
int despachar_tarea(void (*Tarea)(void), uint32_t t_wcet, uint32_t t_bcet, uint32_t *wcet, uint32_t *bcet);

uint8_t buf;
volatile uint16_t valor_tick_actual = TIC_LED_DEFAULT;	// Por default 512ms

int main(void) {

	InitHardware();
	int falla=0;
	uint32_t wcets[3] = {0,0,0};
	uint32_t bcets[3] = {0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF};

	/*
	 * 512 - 01 0000 0000 -> /64 -> 0000 1000
	 * 256 - 00 1000 0000 -> /64 -> 0000 0100
	 * 128 - 00 0100 0000 -> /64 -> 0000 0010
	 * 64  - 00 0010 0000 -> /64 -> 0000 0001
	 */

	leer_24lc64(EEPROM_ADDRESS,1,&buf);
	if ( buf == 0xFF )
	{
		valor_tick_actual  = TIC_LED_DEFAULT;
		buf = TIC_LED_DEFAULT/64;	// 0000 1000
		escribir_24lc64(EEPROM_ADDRESS,1,&buf);
	}
	else {
		valor_tick_actual = buf*64;
	}


	while(1)
    {
    	falla = 0;
			falla += despachar_tarea(TareaTickLed, 240, 0, &wcets[0], &bcets[0]); // mejor valor: 0,47us; peor valor: 2,33us
			falla += despachar_tarea(TareaBtnLed, 190, 0, &wcets[1], &bcets[1]); // mejor valor: 1,04us; peor valor: 1,78us
			falla += despachar_tarea(TareaBtnEeprom, 39000, 0, &wcets[2], &bcets[2]); // mejor valor: 1,21us; peor valor: 387,01us

    	if(falla)
    	{
				for(;;)
				{
					Chip_GPIO_SetPinOutLow(LPC_GPIO,LED_PORT,LED_PIN);
				}
    	}
    	__WFI();
		/*Pongo al sistema en falla*/
    }
    return 0 ;
}

void TareaTickLed(void)
{
	valor_tick_actual--;
	if(!valor_tick_actual)
	{
		valor_tick_actual = buf*64;	// Multiplico x 64 para obtener el valor convertido de ticks
		Chip_GPIO_SetPinToggle(LPC_GPIO,LED_PORT,LED_PIN);
	}
}

void TareaBtnLed(void)
{
	static uint32_t tics = 0;
	static bool last_pul_state = true;
	bool pul_state = Chip_GPIO_GetPinState(LPC_GPIO,BTN_LED_PORT,BTN_LED_PIN);
	// antirebote
	if ( pul_state != last_pul_state ) {
		last_pul_state = pul_state;
		tics = 0;
	}
	else if ( tics < (DEBOUNCE) ) {
		tics++;
		// Paso la cantidad de estados estables
		if ( tics == DEBOUNCE ) {
			// si presiono el pulsador (0 logico)
			if ( pul_state == false ) {
				if(buf == 0x01){
					valor_tick_actual = 512;
					buf = 0x08;
				}
				else{
					buf = buf/2;
					valor_tick_actual = buf*64;
				}

			}
		}
	}
}

void TareaBtnEeprom(void)
{
	static uint32_t tics = 0;
	static bool last_pul_state = true;
	bool pul_state = Chip_GPIO_GetPinState(LPC_GPIO,BTN_EEPROM_PORT,BTN_EEPROM_PIN);
		// Paso la cantidad de estados estables
	if ( pul_state != last_pul_state ) {
		last_pul_state = pul_state;
		tics = 0;
	}
	else if ( tics < (DEBOUNCE) ) {
		tics++;
		// primer flanco del antirebote
		if ( tics == DEBOUNCE ) {
			// si presiono el pulsador (0 logico)
			if ( pul_state == false ) {
				escribir_24lc64(EEPROM_ADDRESS,1,&buf);
			}
		}
	}
}



void SetPinMode ( uint8_t puerto , uint8_t bit , uint8_t modo )
{
	volatile uint32_t *p = ( volatile uint32_t * )  0x4002C040 ;

	*( p + puerto * 2 + bit / 16 ) = *( p + puerto * 2 + bit / 16 ) & ~( 0x3 << (2 * bit));
	*( p + puerto * 2 + bit / 16 ) = *( p + puerto * 2 + bit / 16 ) | ( modo << (2 * (bit % 16)));

}

void InitHardware(void)
{
	/*Inicializo el Clock del sistema*/
	Chip_SetupXtalClocking();
	Chip_SYSCTL_SetFLASHAccess(FLASHTIM_100MHZ_CPU);
	SystemCoreClockUpdate();

	/*Inicializo los perifericos de pines e IOCON*/
	Chip_GPIO_Init(LPC_GPIO);
	Chip_IOCON_Init(LPC_IOCON);

	/*Inicializo el led rojo (para debug) el led a usar por GPIO y los dos botones */
	// Led Rojo
	Chip_IOCON_PinMuxSet(LPC_IOCON,LED_ROJO_PORT,LED_ROJO_PIN,IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,LED_ROJO_PORT,LED_ROJO_PIN);
	// Led TP
	Chip_IOCON_PinMuxSet(LPC_IOCON,LED_PORT,LED_PIN,IOCON_FUNC0);
	Chip_GPIO_SetPinDIROutput(LPC_GPIO,LED_PORT,LED_PIN);
	// Boton EEPROM
	Chip_IOCON_PinMuxSet(LPC_IOCON,BTN_EEPROM_PORT,BTN_EEPROM_PIN,IOCON_FUNC0);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO,BTN_EEPROM_PORT,BTN_EEPROM_PIN);
	SetPinMode(BTN_EEPROM_PORT,BTN_EEPROM_PIN,0); // Configuro como pullup
	// BOTON LED
	Chip_IOCON_PinMuxSet(LPC_IOCON,BTN_LED_PORT,BTN_LED_PIN,IOCON_FUNC0);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO,BTN_LED_PORT,BTN_LED_PIN);
	SetPinMode(BTN_LED_PORT,BTN_LED_PIN,0); // Configuro como pullup


	// Default leds apagados
	Chip_GPIO_SetPinOutLow(LPC_GPIO,LED_PORT,LED_PIN);
	Chip_GPIO_SetPinOutLow(LPC_GPIO,LED_ROJO_PORT,LED_ROJO_PIN);

	/*Inicializo el I2C*/
	Chip_IOCON_PinMux(LPC_IOCON, I2C_DEVICE_PORT, I2C_SDA_PIN, IOCON_MODE_INACT, IOCON_FUNC3);
	Chip_IOCON_PinMux(LPC_IOCON, I2C_DEVICE_PORT, I2C_SCL_PIN, IOCON_MODE_INACT, IOCON_FUNC3);
	Chip_IOCON_EnableOD(LPC_IOCON,I2C_DEVICE_PORT, I2C_SDA_PIN);
	Chip_IOCON_EnableOD(LPC_IOCON,I2C_DEVICE_PORT, I2C_SDA_PIN);
	Chip_I2C_Init(I2C_DEVICE_NUM);
	Chip_I2C_SetClockRate(I2C_DEVICE_NUM,I2C_SPEED);
	Chip_I2C_SetMasterEventHandler(I2C_DEVICE,Chip_I2C_EventHandlerPolling);

	/*Inicializo el systick*/
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock/TIC_1MS);

}

/*
 * Lee la memoria 24xx64
 * 		- address: 	La dirección de la memoria EEPROM a leer (de 0 a 8191).
 * 		- len:		La cantidad de bytes a leer de la memoria
 * 		- buf:		buffer donde pone los datos leidos.
 *
 * 		* devuelve la cantidad de datos que pudo leer.
 */
uint32_t leer_24lc64(uint16_t address, int len, uint8_t *buf)
{
	I2C_XFER_T xfer = {0};
	uint8_t cmd[2];
	cmd[0] = (uint8_t)((address>>8)&0x1F);
	cmd[1] = (uint8_t)(address&0xFF);
	xfer.slaveAddr = 0x50;
	xfer.txBuff = cmd;
	xfer.txSz = 2;
	xfer.rxBuff = buf;
	xfer.rxSz = len;
	while (Chip_I2C_MasterTransfer(I2C_DEVICE, &xfer) == I2C_STATUS_ARBLOST) {}
	return len - xfer.rxSz;
}


/*
 * Escribe la memoria 24LC64.
 * 		- address: 	La dirección de la memoria EEPROM a partir de la cual se
 * 					escriben los datos (de 0 a 8191).
 * 		- len:		La cantidad de datos a escribir (entre 0 y 32) ya que
 * 					no se pueden escribir más de 32 datos en una única
 * 					operación. http://ww1.microchip.com/downloads/en/devicedoc/21189f.pdf
 * 		- buf:		El buffer con los datos a escribir.
 *
 * 		* devuelve la cantidad de datos que no pudo escribir (0 si está todo bien)
 */
uint32_t escribir_24lc64(uint16_t address, uint32_t len, uint8_t *buf)
{
	I2C_XFER_T xfer = {0};
	uint8_t cmd[34];
	cmd[0] = (uint8_t)((address>>8)&0x1F);
	cmd[1] = (uint8_t)(address&0xFF);
	if(len>32) return -1;
	memcpy(&cmd[2],buf,len);
	xfer.slaveAddr = 0x50;
	xfer.txBuff = cmd;
	xfer.txSz = len+2;
	xfer.rxBuff = NULL;
	xfer.rxSz = 0;
	while (Chip_I2C_MasterTransfer(I2C_DEVICE, &xfer) == I2C_STATUS_ARBLOST) {}
	return xfer.txSz;
}

int despachar_tarea(void (*Tarea)(void), uint32_t t_wcet, uint32_t t_bcet, uint32_t *wcet, uint32_t *bcet) {
	uint32_t cuenta;
	dwt_reset();
	Tarea();
	cuenta  = dwt_read();
	if ( cuenta > *wcet ) *wcet = cuenta;
	if ( cuenta < *bcet ) *bcet = cuenta;
	return ((cuenta>t_wcet)||(cuenta<t_bcet))?(-1):0;
}



void SysTick_Handler(void)
{
}
